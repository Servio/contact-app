import Vue from 'vue'
import Router from 'vue-router'
import Show from './views/Show.vue'
import ContactList from './components/ContactList.vue'
import Add from './views/Add.vue'
import Update from './views/Update.vue'
import PrefList from './views/PrefList.vue'
import BanList from './components/BanList.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/',
      name: 'ContactList',
      component: ContactList
    },
    {
      path: '/pref',
      name: 'PrefList',
      component: PrefList
    },
    {
      path: '/banlist',
      name: 'BanList',
      component: BanList
    },
    {
      path: '/add',
      name: 'Add',
      component: Add
    },
    {
      path: '/update/:id',
      name: 'Update',
      component: Update,
      params: true
    },
    {
      path: '/Show/:id',
      name: 'Show',
      component: Show,
      params: true
    }
  ]
})
