import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    contacts: [
      {
        id: '0',
        fname: 'ciccio',
        sname: 'boi',
        descr: ' baby ',
        number: Math.random(),
        pref: 1,
        banned: 1,
        url: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAHBg4QBxAPEBAJDRYLFxUYCBsQCQ0OIB0XGCARFh8kHSgsJCYxJx8fLTEtMTUrOjMxIyszRDwuNzQtLjIBCgoKDQ0NDg8PFi0ZExkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAMMAtAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EAD8QAAICAQICBAoGCAcAAAAAAAABAgMEBRESIRMiMUEVQlFjcYGRoaPiBhQzYcHRI1JicqKx0uEkMjRDkrLx/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD6wADKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRk5KoXPm33fmVt2RK59d8vJ2RAsbcyFfY+J/dz95GlqLb6kV7WyEAqX4Qn5I+x/mZLUZd8Y+1ohACxhqKb68WvXuSqrVat63v/wBkUhtxrehuTfZ2P70BcghvUI+SXuJNNyuhvX6P2kEZgAAAAAAAAAAAAAAAGF9nQ1NvuXtZmQdUntGKXe3ICBObsm3Ltk9zwAigAAAAAAABvxLuhu59kuq/zNAKL4GrEn0mPFvybezkbQgAAAAAAAAAAAAAFZqb/Tx+6P4lmVmp/wCoX7q/mwqIACAAAAAAAAAAALPTXvjeiTJZC0t/o5fvL+RNKgAAAAAAAAAAAAAFZqa/Tr74/iyzImo18VXEvEfuYFYACKAAAAAAAAAACx0tdSf7y/kTSNgV9HjLfx3xfkSSoAAAAAAAAAAAAABWZ90ldKKfLhS28V8izKvUY7ZO/wCsk/wCooAIAAAAAAAAB7F7S5rfb/izwAXdFnS1Jrxl7DM04keHGhv+rv7eZuKgAAAAAAAAAAAAAELU696k14j29TJphdX0tUk/GW3rApAbcjHdG3Ht1t+w1EUAAAAAAAAPYR45pfrNI8JuHiPihNtbbcX7RRYpbLl3cgAEAAAAAAAAAAAAAAAARs+rpKN1219b1d5VF96Slya+iua8j39QVrABAAAAAAexXFJJdsnsXdcejgkvFSRX6bXxWuT8RbetlkUAAEAAAAAAAAAAAAAAAACu1RbTi/LFr2f+liV2qS3sivIt/a/7AQgARQAAAABZ6YtqH98vwJZE01/4b0Sf4EsqAAAAAAAAAAAAAAAAAPG9lu+4iXZ8Y/Z9b3RAmN7Ln3FNl2dLe2uzs9SF2RK7/O+Xk7ImoKAAgAAAAAJen3quTjPsnz9DLMoTfTlzpWye6Xcyi3BEr1CMvtE4++JJhNTXUafrCMgAAAAAAAADRk5KoXlfk/MDc2ord8kvvIl2eo/ZdZ+yJBuuldLex/0o1hWdt0rn+kf9K9RgAQAAAAAAAAAAAAAA9i3F7xez9Ox4AJdWfKH2nWXskTqciNy6j5+TskUwT2fIovgVlGdKvlZ1l/EiwqsjbDet/wBgjMAASp4DcXwT2b7+j3295Ceg7vd29vP7Ht/iLwFmIo/AHnfg/MPAHnfg/MXgExao3oG3+78H5iDhY+Nn2yjg5tFs61vKMLYznDu5pTbXMsfpVVK7RZxhGU49JVKyEYuU7cZWQdkElze8OJbLtXLvOM0SFNuRjQ02ul5ePbRx315cbJ2uM97b5qMnwJ18a66i25qCXJCYV0mo4uPpcFLU8yiiM3wp2WRqjJ+RNzW7JNeiK2ClXcpRmlJNVbpp9jT4uwrs7Nx9J+mF9v0glXVC/Dprots2WPsnb0lMW+Sk24trtktu3blSavn1x03Er09XYVdmPk3UwlrNmDCfXSg4KEXOc2nxQq3S4ZbNdiSYV1/gDzvwfmNGbplWBjuzOya6a4ds57Qqj6W5JI5XKzLc/TMvKjlZHFhaDiahX0efKGP9ZayJSskotKW7gk090+9dm3TfTV1Qrw55lssf6tl9NC90RswMezgsinem9lFqTSfLaTjzQmFKMKjKqqnjZdNkMuTrrlGcZwuklKTjBqWze0W9l3Rb7mS/AHnfg/McTbkxyrsa6noW69ck430X2Y+l6hkPDu4ZJcbW7moQezkpPeO74pJ5UapYsGT0LKysib0mV2U5XyvtxMvipSai9+jns7nwJJdRdXlzTCuvno8a5wjZfFSubjFOtKc2k3tFcXclvy7kYZ2m1afjStzsmumqvbinPaFEN2kt25JLd7L0s5TUbZ33UL6GZU8mSvtUJ25jyKIWvGsbjVbLibfZybajJ9m28S61/Ijm/QCmemTk1ZdhcErXK21S+sULa3eW7aaakm0901uhMKkYdeJnU8eFnY1sFZGnihfCcFY2lGG6m1u20ku1tlj4A878H5jnNe0C6EZZGpZEJXZuXp+JvRh/VoQqjkxfFznNuW83z35bLZGvIyPqd0qM/KyKsHH1adE7ZapZG+uv6vXbCqdzlxqLnJ820+Sjvs9mmFdFLR4xvjW74qc4uaj0aVsoRcVKSXFu0nKKb7t15UbfAHnfg/McLmZNqjh5Cuvd3g7U6qJu+SyLqI30dHYob7Tm6d57NNy4VJpuK26v6I5MLtTzI6XkW5WFXVRKNks2WTFZTdvSVxnJtvZKttbvhba5diTCp/gDzvwfmHgDzvwfmLwCYVR+APO/B+Y2Y+jOizdW/dt0O2/vLgCYVC+o/tfwf3BNAmIAAoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/9k'
      },
      {
        id: '1',
        fname: 'troll',
        sname: 'xp',
        descr: 'ioid',
        number: Math.random(),
        pref: 0,
        banned: 0,
        url: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAHBg4QBxAPEBAJDRYLFxUYCBsQCQ0OIB0XGCARFh8kHSgsJCYxJx8fLTEtMTUrOjMxIyszRDwuNzQtLjIBCgoKDQ0NDg8PFi0ZExkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAMMAtAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EAD8QAAICAQICBAoGCAcAAAAAAAABAgMEBRESIRMiMUEVQlFjcYGRoaPiBhQzYcHRI1JicqKx0uEkMjRDkrLx/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD6wADKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRk5KoXPm33fmVt2RK59d8vJ2RAsbcyFfY+J/dz95GlqLb6kV7WyEAqX4Qn5I+x/mZLUZd8Y+1ohACxhqKb68WvXuSqrVat63v/wBkUhtxrehuTfZ2P70BcghvUI+SXuJNNyuhvX6P2kEZgAAAAAAAAAAAAAAAGF9nQ1NvuXtZmQdUntGKXe3ICBObsm3Ltk9zwAigAAAAAAABvxLuhu59kuq/zNAKL4GrEn0mPFvybezkbQgAAAAAAAAAAAAAFZqb/Tx+6P4lmVmp/wCoX7q/mwqIACAAAAAAAAAAALPTXvjeiTJZC0t/o5fvL+RNKgAAAAAAAAAAAAAFZqa/Tr74/iyzImo18VXEvEfuYFYACKAAAAAAAAAACx0tdSf7y/kTSNgV9HjLfx3xfkSSoAAAAAAAAAAAAABWZ90ldKKfLhS28V8izKvUY7ZO/wCsk/wCooAIAAAAAAAAB7F7S5rfb/izwAXdFnS1Jrxl7DM04keHGhv+rv7eZuKgAAAAAAAAAAAAAELU696k14j29TJphdX0tUk/GW3rApAbcjHdG3Ht1t+w1EUAAAAAAAAPYR45pfrNI8JuHiPihNtbbcX7RRYpbLl3cgAEAAAAAAAAAAAAAAAARs+rpKN1219b1d5VF96Slya+iua8j39QVrABAAAAAAexXFJJdsnsXdcejgkvFSRX6bXxWuT8RbetlkUAAEAAAAAAAAAAAAAAAACu1RbTi/LFr2f+liV2qS3sivIt/a/7AQgARQAAAABZ6YtqH98vwJZE01/4b0Sf4EsqAAAAAAAAAAAAAAAAAPG9lu+4iXZ8Y/Z9b3RAmN7Ln3FNl2dLe2uzs9SF2RK7/O+Xk7ImoKAAgAAAAAJen3quTjPsnz9DLMoTfTlzpWye6Xcyi3BEr1CMvtE4++JJhNTXUafrCMgAAAAAAAADRk5KoXlfk/MDc2ord8kvvIl2eo/ZdZ+yJBuuldLex/0o1hWdt0rn+kf9K9RgAQAAAAAAAAAAAAAA9i3F7xez9Ox4AJdWfKH2nWXskTqciNy6j5+TskUwT2fIovgVlGdKvlZ1l/EiwqsjbDet/wBgjMAASp4DcXwT2b7+j3295Ceg7vd29vP7Ht/iLwFmIo/AHnfg/MPAHnfg/MXgExao3oG3+78H5iDhY+Nn2yjg5tFs61vKMLYznDu5pTbXMsfpVVK7RZxhGU49JVKyEYuU7cZWQdkElze8OJbLtXLvOM0SFNuRjQ02ul5ePbRx315cbJ2uM97b5qMnwJ18a66i25qCXJCYV0mo4uPpcFLU8yiiM3wp2WRqjJ+RNzW7JNeiK2ClXcpRmlJNVbpp9jT4uwrs7Nx9J+mF9v0glXVC/Dprots2WPsnb0lMW+Sk24trtktu3blSavn1x03Er09XYVdmPk3UwlrNmDCfXSg4KEXOc2nxQq3S4ZbNdiSYV1/gDzvwfmNGbplWBjuzOya6a4ds57Qqj6W5JI5XKzLc/TMvKjlZHFhaDiahX0efKGP9ZayJSskotKW7gk090+9dm3TfTV1Qrw55lssf6tl9NC90RswMezgsinem9lFqTSfLaTjzQmFKMKjKqqnjZdNkMuTrrlGcZwuklKTjBqWze0W9l3Rb7mS/AHnfg/McTbkxyrsa6noW69ck430X2Y+l6hkPDu4ZJcbW7moQezkpPeO74pJ5UapYsGT0LKysib0mV2U5XyvtxMvipSai9+jns7nwJJdRdXlzTCuvno8a5wjZfFSubjFOtKc2k3tFcXclvy7kYZ2m1afjStzsmumqvbinPaFEN2kt25JLd7L0s5TUbZ33UL6GZU8mSvtUJ25jyKIWvGsbjVbLibfZybajJ9m28S61/Ijm/QCmemTk1ZdhcErXK21S+sULa3eW7aaakm0901uhMKkYdeJnU8eFnY1sFZGnihfCcFY2lGG6m1u20ku1tlj4A878H5jnNe0C6EZZGpZEJXZuXp+JvRh/VoQqjkxfFznNuW83z35bLZGvIyPqd0qM/KyKsHH1adE7ZapZG+uv6vXbCqdzlxqLnJ820+Sjvs9mmFdFLR4xvjW74qc4uaj0aVsoRcVKSXFu0nKKb7t15UbfAHnfg/McLmZNqjh5Cuvd3g7U6qJu+SyLqI30dHYob7Tm6d57NNy4VJpuK26v6I5MLtTzI6XkW5WFXVRKNks2WTFZTdvSVxnJtvZKttbvhba5diTCp/gDzvwfmHgDzvwfmLwCYVR+APO/B+Y2Y+jOizdW/dt0O2/vLgCYVC+o/tfwf3BNAmIAAoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/9k'
      },
      {
        id: '2',
        fname: 'jonas',
        sname: 'trugman',
        descr: 'fdsdsd ',
        number: Math.random(),
        pref: 0,
        banned: 0,
        url: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAHBg4QBxAPEBAJDRYLFxUYCBsQCQ0OIB0XGCARFh8kHSgsJCYxJx8fLTEtMTUrOjMxIyszRDwuNzQtLjIBCgoKDQ0NDg8PFi0ZExkrKysrKystLSsrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAMMAtAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAUCAwYBB//EAD8QAAICAQICBAoGCAcAAAAAAAABAgMEBRESIRMiMUEVQlFjcYGRoaPiBhQzYcHRI1JicqKx0uEkMjRDkrLx/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD6wADKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRk5KoXPm33fmVt2RK59d8vJ2RAsbcyFfY+J/dz95GlqLb6kV7WyEAqX4Qn5I+x/mZLUZd8Y+1ohACxhqKb68WvXuSqrVat63v/wBkUhtxrehuTfZ2P70BcghvUI+SXuJNNyuhvX6P2kEZgAAAAAAAAAAAAAAAGF9nQ1NvuXtZmQdUntGKXe3ICBObsm3Ltk9zwAigAAAAAAABvxLuhu59kuq/zNAKL4GrEn0mPFvybezkbQgAAAAAAAAAAAAAFZqb/Tx+6P4lmVmp/wCoX7q/mwqIACAAAAAAAAAAALPTXvjeiTJZC0t/o5fvL+RNKgAAAAAAAAAAAAAFZqa/Tr74/iyzImo18VXEvEfuYFYACKAAAAAAAAAACx0tdSf7y/kTSNgV9HjLfx3xfkSSoAAAAAAAAAAAAABWZ90ldKKfLhS28V8izKvUY7ZO/wCsk/wCooAIAAAAAAAAB7F7S5rfb/izwAXdFnS1Jrxl7DM04keHGhv+rv7eZuKgAAAAAAAAAAAAAELU696k14j29TJphdX0tUk/GW3rApAbcjHdG3Ht1t+w1EUAAAAAAAAPYR45pfrNI8JuHiPihNtbbcX7RRYpbLl3cgAEAAAAAAAAAAAAAAAARs+rpKN1219b1d5VF96Slya+iua8j39QVrABAAAAAAexXFJJdsnsXdcejgkvFSRX6bXxWuT8RbetlkUAAEAAAAAAAAAAAAAAAACu1RbTi/LFr2f+liV2qS3sivIt/a/7AQgARQAAAABZ6YtqH98vwJZE01/4b0Sf4EsqAAAAAAAAAAAAAAAAAPG9lu+4iXZ8Y/Z9b3RAmN7Ln3FNl2dLe2uzs9SF2RK7/O+Xk7ImoKAAgAAAAAJen3quTjPsnz9DLMoTfTlzpWye6Xcyi3BEr1CMvtE4++JJhNTXUafrCMgAAAAAAAADRk5KoXlfk/MDc2ord8kvvIl2eo/ZdZ+yJBuuldLex/0o1hWdt0rn+kf9K9RgAQAAAAAAAAAAAAAA9i3F7xez9Ox4AJdWfKH2nWXskTqciNy6j5+TskUwT2fIovgVlGdKvlZ1l/EiwqsjbDet/wBgjMAASp4DcXwT2b7+j3295Ceg7vd29vP7Ht/iLwFmIo/AHnfg/MPAHnfg/MXgExao3oG3+78H5iDhY+Nn2yjg5tFs61vKMLYznDu5pTbXMsfpVVK7RZxhGU49JVKyEYuU7cZWQdkElze8OJbLtXLvOM0SFNuRjQ02ul5ePbRx315cbJ2uM97b5qMnwJ18a66i25qCXJCYV0mo4uPpcFLU8yiiM3wp2WRqjJ+RNzW7JNeiK2ClXcpRmlJNVbpp9jT4uwrs7Nx9J+mF9v0glXVC/Dprots2WPsnb0lMW+Sk24trtktu3blSavn1x03Er09XYVdmPk3UwlrNmDCfXSg4KEXOc2nxQq3S4ZbNdiSYV1/gDzvwfmNGbplWBjuzOya6a4ds57Qqj6W5JI5XKzLc/TMvKjlZHFhaDiahX0efKGP9ZayJSskotKW7gk090+9dm3TfTV1Qrw55lssf6tl9NC90RswMezgsinem9lFqTSfLaTjzQmFKMKjKqqnjZdNkMuTrrlGcZwuklKTjBqWze0W9l3Rb7mS/AHnfg/McTbkxyrsa6noW69ck430X2Y+l6hkPDu4ZJcbW7moQezkpPeO74pJ5UapYsGT0LKysib0mV2U5XyvtxMvipSai9+jns7nwJJdRdXlzTCuvno8a5wjZfFSubjFOtKc2k3tFcXclvy7kYZ2m1afjStzsmumqvbinPaFEN2kt25JLd7L0s5TUbZ33UL6GZU8mSvtUJ25jyKIWvGsbjVbLibfZybajJ9m28S61/Ijm/QCmemTk1ZdhcErXK21S+sULa3eW7aaakm0901uhMKkYdeJnU8eFnY1sFZGnihfCcFY2lGG6m1u20ku1tlj4A878H5jnNe0C6EZZGpZEJXZuXp+JvRh/VoQqjkxfFznNuW83z35bLZGvIyPqd0qM/KyKsHH1adE7ZapZG+uv6vXbCqdzlxqLnJ820+Sjvs9mmFdFLR4xvjW74qc4uaj0aVsoRcVKSXFu0nKKb7t15UbfAHnfg/McLmZNqjh5Cuvd3g7U6qJu+SyLqI30dHYob7Tm6d57NNy4VJpuK26v6I5MLtTzI6XkW5WFXVRKNks2WTFZTdvSVxnJtvZKttbvhba5diTCp/gDzvwfmHgDzvwfmLwCYVR+APO/B+Y2Y+jOizdW/dt0O2/vLgCYVC+o/tfwf3BNAmIAAoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/9k'
      },
      {
        id: '3',
        fname: 'cotton',
        sname: 'charge',
        descr: ' exxxfmsafm ',
        number: Math.random(),
        pref: 0,
        banned: 0,
        url: 'https://media1.tenor.com/images/e2aefd41f71af78411a431bfbbc1359a/tenor.gif'
      }
    ]
  },
  mutations: {
    addContact: (state, contact) => {
      state.contacts.push(contact)
    },
    removec: (state, contacts) => {
      state.contacts.splice(contacts, 1)
    },
    starContact: (state, index) => {
      if (state.contacts[index].pref !== 1) {
        state.contacts[index].pref = 1
        console.log(state.contacts[index].pref + 'Mutation')
      } else {
        state.contacts[index].pref = 0
        console.log(state.contacts[index].pref + 'Mutation')
      }
    },
    BanContact: (state, index) => {
      if (state.contacts[index].banned !== 1) {
        state.contacts[index].banned = 1
        console.log(state.contacts[index].banned + 'Mutation')
      } else {
        state.contacts[index].banned = 0
        console.log(state.contacts[index].banned + 'Mutation')
      }
    },
    updateContact: (state, contact, pip) => {
      Vue.set(state.contacts, pip, contact)
    }
  },
  actions: {
    remove: (context, contact) => {
      context.commit('removec', contact)
    }
  }
})
